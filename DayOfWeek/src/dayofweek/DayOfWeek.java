package dayofweek;

public class DayOfWeek 
{
	/**
	 * Day of Week class takes in a month, day of month, and year (from 1900-2099), and calculates
	 * the day of the week the date takes place on. 
	 * 
	 * @Hudson
	 */
	
	private int myMonth;
	private int myDayOfMonth;
	private int myYear;
	public static int NO_VALUE = -1;
	
	/**
	 * The adjustment array possesses all of the number offsets for each month of the year
	 * (Used in getNumericDayOfWeek() function)
	 * @Hudson
	 */
	
	private int[] adjustment = {1,4,4,0,2,5,0,3,6,1,4,6};
	
	/**
	 * The month string array returns the name in a String of any given month:
	 *  0 = January, 1 = February, etc.
	 *  (Used in getMonthString() function)
	 *  @Hudson
	 */
	
	private String[] monthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
	
	/**
	 * The dayNames array returns the name in a String relating to the getNumericDayOfWeek function
	 * 0 = Saturday, 1 = Sunday, etc.
	 * (Used in getDayOfWeek function)
	 * @Hudson
	 */
	
	private String[] dayNames = {"Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday"};
	
	/** 
	 * The monthDays array contains the number of days for each month
	 * @Hudson
	 */
	
	private int[] monthDays = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
			
		/**
		 * Constructor for DayOfWeek class
		 * @Hudson
		 */
	
	public DayOfWeek(int month, int dayOfMonth, int year)
	{
		myMonth = month;
		myDayOfMonth = dayOfMonth;
		myYear = year;
	}
	
	/**
	 * Returns the Numeric Day of the Week for the given month, day, and year
	 * @Hudson
	 */
	
	public int getNumericDayOfWeek() 
	{
		
		int numDayOfWeek;
		int adjYear = myYear - 1900;
		
		if (checkValid()) 
		{
			numDayOfWeek = adjustment[myMonth - 1] + myDayOfMonth + adjYear + (adjYear / 4);
		
			if (adjYear != 0 && adjYear % 4 == 0 && myMonth <= 2) 
			{
				numDayOfWeek -= 1;
			}

			return numDayOfWeek % 7;
		} else return -1;
	}
	
	/**
	 * Uses the dayNames String and the getNumericDayOfWeek function to determine the name of the day of the week
	 * @Hudson
	 */
	
	public String getDayOfWeek() 
	{
		if (checkValid()) 
		{
		return dayNames[this.getNumericDayOfWeek()];
		} else return null;
	}
	/**
	 * checks to see if date given is valid
	 * @Hudson
	 */
	
	private boolean checkValid() 
	{
		int adjYear = myYear - 1900; 
		if (adjYear >= 0 && adjYear <= 199 && myMonth >= 1 && myMonth <= 12 && myDayOfMonth >= 1 && myDayOfMonth <= monthDays[myMonth - 1]) 
		{
			return true; 
		} 
		else if (myMonth == 2 && myYear % 4 == 0 && myDayOfMonth == 29)
		{
			return true;
		} 
		else return false;
		
	}
	
	public int getMonth() 
	{
		if (checkValid()) 
		{
		return myMonth;
		}
		else return -1;
	}
	
	/**
	 * Uses monthNames String and the getMonth() function to determine the name of the month
	 * @Hudson
	 */
	
	public String getMonthString() 
	{
		if (checkValid()) 
		{
		return monthNames[this.getMonth() - 1];
		} 
		else return null;
	}
	
	public int getDayOfMonth()
	{
		if (checkValid()) 
		{
		return myDayOfMonth;
		}
		else return -1;
	}
	
	public int getYear() 
	{
		if (checkValid()) 
		{
		return myYear;
		}
		else return -1;
	}
	
}
