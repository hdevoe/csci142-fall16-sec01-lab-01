package dayofweek;
/**

 * Required API to conform to is:
 * 
 * public DayOfWeek(int month, int dayOfMonth, int year) constructor (where year is 1975, not 75!)
 * public int getNumericDayOfWeek() (where Saturday=0, Friday=6, NO_VALUE=-1)
 * public String getDayOfWeek() (returns "Saturday", "Sunday", ..., "Friday", null)

 * public int getMonth() (where January=1, December=12)
 * public String getMonthString()
 * public int getDayOfMonth()
 * public int getYear() (for example, return 1975 for the year, not 75)
 */

public class Main 
{
	
	
	
	public static void main(String[] args) 
	{
		DayOfWeek myDayOfWeek = new DayOfWeek(1, 1, 1900);
		
		System.out.println(myDayOfWeek.getNumericDayOfWeek() + " " + myDayOfWeek.getDayOfWeek());
	}
}
