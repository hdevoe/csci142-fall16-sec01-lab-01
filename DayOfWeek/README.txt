Hudson DeVoe
Lab 1.11

1. True

2. The array of names would have to be shifted so that Sunday appears first and Saturday appears last; if it is > 0, the NumericDayOfWeek algorithm would have to be subracted by one after it is run through mod 7, and if it is 0, then it would have to be changed to a 6 so that it equals the "Saturday" index of the String[] DaysOfTheWeek array.

3. You subtract one in a leap year if the date is in January or February because the leap day (Feb. 29) has yet to occur.

4. The remainder associated with 2016 is 0 because 2016 % 7 == 0.
